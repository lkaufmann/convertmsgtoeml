#!/bin/bash

docker run --rm --name tmpUbuntu -v $(pwd):/work -ti -d ubuntu
docker exec -it tmpUbuntu apt-get update
docker exec -it tmpUbuntu  apt-get install -y libemail-outlook-message-perl
docker exec -it tmpUbuntu apt-get install -y wget
docker exec -it tmpUbuntu wget http://www.matijs.net/software/msgconv/msgconvert.pl

docker exec -it tmpUbuntu bash -c "cd /work && find -iname *.msg -exec msgconvert --outfile {}.eml {}  \;"

docker stop tmpUbuntu
