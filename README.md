# ConvertMsgToEml

Script pour convertir les fichiers mails, présent dans le dossier courant, au format .msg (Outlook) vers le format générique .eml.  

Utilise  https://www.matijs.net/software/msgconv/ et Docker